<?php
// include 'common.php';
include 'header.php';
include 'menu.php';
?>

<style>
    .list input {
        border: none;
        font-size: 15px;
    }

    .list .head th {
        border-bottom: 2px solid #eee;
    }

    .list tbody td {
        border-bottom: 1px solid #eee;
    }


    .list .content {
        height: 50px;
    }

</style>

<head>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>


<?php
$prefix = $db->getPrefix();
$vists = $db->fetchAll(
    $db->select()->from($prefix . 'visits')->order($prefix . 'visits.id', Typecho_Db::SORT_DESC));

?>

<div class="main" align="center">
    <p>
    <h2>管理网站访问者位置信息</h2></p>
    <p>
    <table class="list">
        <colgroup>
            <col width="50"/>
            <col width="100"/>
            <col width="150"/>
            <col width="250"/>
            <col width="200"/>
        </colgroup>

        <thead>
        <tr class="head" style="height: 50px">
            <th>ID</th>
            <th>IP</th>
            <th>位置</th>
            <th>访问页面</th>
            <th>时间</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($vists as $vist): ?>
            <tr align="center" class="content">
                <td><label><?php echo $vist['id']; ?></label></td>
                <td><label><?php echo $vist['ip']; ?></label></td>
                <td><label><?php echo $vist['location']; ?></label></td>
                <td><label><?php echo $vist['page']; ?></label></td>
                <td><label><?php echo $vist['time']; ?></label></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
    </p>

</div>